terraform {
 required_providers {
   aws = {
     source  = "hashicorp/aws"
     version = "~> 4.0"
   }
 }
}

provider "aws" {
  access_key = ""
  secret_key = ""
  region = "us-east-1"
}
# resource "aws_security_group" "demo-sg" {
#   name = "sec-grp"
#   description = "Allow HTTP and SSH traffic via Terraform"

#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

resource "aws_cloudfront_distribution" "example" {
  origin {
    domain_name = "example.com"
    origin_id   = "example"
  }

  default_cache_behavior {
    target_origin_id = "example"
    viewer_protocol_policy = "redirect-to-https"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    forwarded_values {
      query_string = false
    }
  }
  enabled = true
  lambda_function_association {
    event_type = "viewer-request"
    lambda_arn = aws_lambda_function.example.arn
  }
}

resource "aws_iam_role" "example" {
  name = "example_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "example" {
  name = "example_policy"
  role = aws_iam_role.example.id
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudfront:UpdateDistribution"
            ],
            "Resource": "arn:aws:cloudfront:::distribution/*"
        }
    ]
}
EOF
}

resource "aws_lambda_function" "example" {
  function_name = "example-function"
  runtime = "nodejs12.x"
  handler = "index.handler"
  role = aws_iam_role.example.arn
  filename = "function.zip"
  source_code_hash = filebase64sha256("function.zip")
}

